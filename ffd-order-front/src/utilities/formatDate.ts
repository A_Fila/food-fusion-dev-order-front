const DATE_FORMATTER = new Intl.DateTimeFormat("RU",
    {
        day: "numeric",
        year: "2-digit",
        month: "2-digit",
        hour: "2-digit",
        hour12: false,
        minute: "2-digit"
    });

const DATE_FORMATTER_LONG = new Intl.DateTimeFormat("RU",
    {
        day: "numeric",
        year: "2-digit",
        month: "2-digit",
        hour: "2-digit",
        hour12: false,
        minute: "2-digit",
        second: "2-digit",
    });

export function formatDate(date: Date) {
    return DATE_FORMATTER.format(date).replace(',','');
}
export function formatDateLong(date: Date) {
    return DATE_FORMATTER_LONG.format(date).replace(',', '');
}