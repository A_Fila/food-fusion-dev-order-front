import { Route, Routes } from "react-router-dom"
import { Home } from "./pages/Home"
import { Container } from "react-bootstrap"

import { About } from "./pages/About"
import { Navbar } from "./components/Navbar"
import { OrderListProvider } from "./context/OrderListContext"
import { Order } from "./pages/Order"



function App() {
  
    return (        
      <OrderListProvider>
          <Navbar />
          <Container>
              <Routes>
                    <Route path="/" element={<Home />} />
                    <Route path="/order" element={<Order />} />
                    <Route path="/about" element={<About />} />
              </Routes>
          </Container>
      </OrderListProvider>
  )
}

export default App