import React, { useState } from 'react';
import axios, { AxiosRequestConfig, AxiosResponse, Method } from 'axios';
//import { $realApi } from '@/services/api';
import { IResponseModel } from '../interfaces/common';//import { IResponseModel } from '@/interfaces/common';
import { $realApi } from './api';

interface UseApiHook<T> {
    data: T | null;
    error: any;
    loading: boolean;
    success: boolean;
    fetchData: (data?: any) => Promise<void>;
}

export const useApi = <T>(url: string, method: Method = 'GET'): UseApiHook<T> => {
    // const url = url;
    const [data, setData] = useState<T | null>(null);
    const [error, setError] = useState<any>(null);
    const [success, setSuccess] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const fetchData = async (data?: any) => {
        setSuccess(false);
        setError(null);
        setLoading(true);
        try {
            console.log('url=', url)
            const response: AxiosResponse<IResponseModel<T>> = await $realApi({ url, method, data });
            console.log('useApi() response=', response)
            setData(response.data);//.body
            setSuccess(response.data.result == 200);
            //setLoading(false);
            // return response.data.body
        } catch (error) {
            setError(error);
            setSuccess(false);
            //setLoading(false);
            // return error
        } finally {
            setLoading(false);
        }
    };
    const fetcher = { fetchData, data, error, success, loading };
    return fetcher;
    // return { data, error, success, loading, fetchData };
};
