import { useState, useEffect } from 'react';
import { AxiosError, AxiosRequestConfig, AxiosResponse, Method, AxiosInstance  } from 'axios';
import { formatDateLong } from '../utilities/formatDate';

interface Props {
    axiosInstance: AxiosInstance;
    method: Method;
    url: string;
    requestConfig: AxiosRequestConfig;
}
const useAxios = () => {

    const [response, setResponse] = useState<AxiosResponse>();
    const [error, setError] = useState<AxiosError>();
    const [loading, setLoading] = useState(false);
    const [controller, setController] = useState<AbortController>();

    const axiosFetch = async (config: Props) => {

        try {
            setLoading(true);

            const ctrl = new AbortController();
            setController(ctrl);

            //fetch
            const res = await config.axiosInstance(config.url, {
                method: config.method,
                ...config.requestConfig,
                signal: ctrl.signal
            });

            console.log(`${formatDateLong(new Date())} axiosFetch:${config.axiosInstance.getUri()}`, res);
            setResponse(res.data);
        } catch (err) {
            console.error('axiosFetch', err.message);
            setError(err.message);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        //console.log('useEffect', controller)

        // useEffect cleanup function
        return () => controller && controller.abort();

    }, [controller]);

    return [response, error, loading, axiosFetch];
}

export default useAxios