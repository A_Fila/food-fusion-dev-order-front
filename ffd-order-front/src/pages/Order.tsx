import { Badge, Col, Row } from 'react-bootstrap'
import { OrderItem } from '../components/OrderItem'
import { formatDate } from '../utilities/formatDate'
import { useOrderListContext } from '../context/OrderListContext';
import { OrderDetailStateEnum } from '../enums/orderDetailStateEnum';
import { $realApi } from '../services/api';
import { getOrderUrl } from '../constant/router';
import { useEffect } from 'react';
import { OrderStateEnum } from '../enums/OrderStateEnum';
//import { OrderList } from '../components/OrderList';
//import { Order as OrderType } from '../context/OrderListContext'
//!!!!!!!!!!!!!!!!!!!
//import order from '../data/orderItems.json' //!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!

export function Order() {
    const { curOrderId, order, setCurrentOrder, updateOrderList } = useOrderListContext()
    //const [ order, setOrder ] = useState<Order>();
    //const [currentOrderId, setCurrentOrderId] = useLocalStorage<string>("ffd-order-id", '')
    
    //console.log('orderComponent, order=', order)

    const itemsCount = order?.details?.length;
    const cancelCount = order?.details?.filter(d => d.stateInx === OrderDetailStateEnum.Canceled).length;
    const readyCount = order?.details?.filter(d => d.stateInx === OrderDetailStateEnum.Ready ||
                                                  d.stateInx === OrderDetailStateEnum.Canceled).length;
    const readyProc = readyCount / itemsCount * 100;

    const fetchOrder = async () => {    
        if (!curOrderId) {
            setCurrentOrder(null);
            return;
        }
        try {
            const response = await $realApi.get(`${getOrderUrl}/${curOrderId}`);
            setCurrentOrder(response.data)
            console.log('fetchOrder, response=', response)
            //setCurrentOrder(response.data);
        }
        catch (error) {
            console.error(error);
        }         
    }

    useEffect(() => {
        //100% - update order state
        if (itemsCount && readyCount === itemsCount) {
            console.log('100%!!!!!!!!!!!!!!!!!!!!!!!!')
            const state = (cancelCount === itemsCount) ? OrderStateEnum.Canceled
                                                       : OrderStateEnum.Ready;
            updateOrderList(order.id, state);
        }
    }, [readyProc]);

    useEffect(() => {
        //console.log('OrderComponent, useEffect() order=', order);
        //console.log('order.id=', order.id);
        //console.log('curOrderId=', curOrderId);
        if (!order || order.id !== curOrderId) {
            fetchOrder();
        }
        
    }, [curOrderId])

    return (       
        <>
            {order && (
              <>
                <h3 className="text-secondary">
                        Заказ № <small className="text-primary">{order.number}</small>
                        &nbsp;от <small className="text-primary">{formatDate(new Date(order.date))}</small>

                        <span className="fs-6 text-muted"> (id={order.id})</span>

                        <div className="fs-4">статус: <small className="text-primary">{order?.statusText?.toLocaleLowerCase()}</small></div>
                        <div className="fs-4">приготовлен:&nbsp;
                            <Badge bg={readyProc === 100 ? 'success'
                                                         : 'warning'}
                            >
                                {readyProc}%
                            </Badge>
                        </div>  
                </h3>                    
                   
                    <Row md={2} lg={3} className="g-3">
                        {order?.details?.map(i =>
                            <Col key={i.id}>
                                <OrderItem orderId={order.id} {...i} />
                            </Col>
                        )}
                </Row>
              </>
            )}
        </> 
    )
}