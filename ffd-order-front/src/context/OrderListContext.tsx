import React, { useEffect, useState } from "react"
import { OrderList } from "../components/OrderList"
import useAxios from "../hooks/useAxios";
import axios from '../api/OrderDetailStatesController';
import { OrderDetailStateEnum } from "../enums/orderDetailStateEnum";
import { useApi } from "../services/caller";
import { getOrderDetailStatesUrl, getOrderListUrl  } from "../constant/router";
import { formatDateLong } from "../utilities/formatDate";
import { $realApi } from "../services/api";
import { useLocalStorage } from "../hooks/useLocalStorage";

type OrderListProviderProps = {
	children:React.ReactNode
}

type OrderListContextType = {
	//ctxOrderId : string
	nextItemState: (id: string) => void
	prevItemState: (id: string) => void
	cancelItemState: (id: string) => void

	getStateDescription: (inx:number) => string

	openOrderList: () => void
	closeOrderList: () => void

	order: Order | null
	setCurrentOrder: (order: Order | null) => void

	curOrderId: string
	setCurOrderId: (orderId: string) => void

	orderList: OrderShort[] | null
	//setOrderList: (list: OrderShort[]) => void
	updateOrderList: (orderId: string, status: number) => void
}
 
export type Order = {
	id: string;
	number: number;
	date: Date
	total: number
	status: string;
	statusText: string;
	details: OrderItem[]
}

type OrderItem = {
	id: string;
	name: string;
	price: number;
	description: string;
	count: number;
	stateText: string;
	stateInx: number;
}

type OrderDetailState = {
	id: number;
	name: string;
	description: string
}
type OrderShort = {
	id: string;
	number: string;
	date: string;
	total: number;
	state: number;
	stateText: string;
}

const OrderListContext = React.createContext<OrderListContextType>({} as OrderListContextType);

export const useOrderListContext = () => {
	const orderListContext = React.useContext(OrderListContext);
	if (!orderListContext) throw new Error('You need to use this OrderListContext inside the provider!')

	return orderListContext;
}

export const OrderListProvider = ({ children }: OrderListProviderProps) => {

	const [curOrderId, setCurOrderId] = useLocalStorage<string>("ffd-order-id", '')
	const [isOpen, setIsOpen] = React.useState(true)	
	const [order, setOrder] = useState<Order | null>(null)
	const [orderList, setOrderList] = useState<OrderShort[] | null>(null)
	const { data: states, error: statesError, success, loading: statesLoading, fetchData: getStates } = useApi<OrderDetailState[]>(getOrderDetailStatesUrl, 'GET');
	//const { data: orderList, error: orderListError, orderListSuccess, loading: orderListLoading, fetchData: getOrders } = useApi<OrderShort[]>(getOrderListUrl, 'GET');

	useEffect(() => {
		getOrders();
		getStates(); //load states
		//console.log('useEffect() states=', states)
	}, [])

	const getOrders = async () => {
		try {
			const response = await $realApi.get(getOrderListUrl);
			setOrderList(response.data);
		}
		catch (error) {
			console.error(error);
		}
	}

	function nextItemState(id: string) {
		const item = order?.details.find(i => i.id === id)
		if (item === null)
			console.error(`Position ${id} not found in Order ${order?.id}`)

		if (item.stateInx >= OrderDetailStateEnum.Ready)
			return; //skip

		updateOrderPosition(id, ++item.stateInx);
	}
	function prevItemState(id: string) {
		
		const item = order?.details.find(i => i.id === id)
		if (item === null)
			console.error(`Position ${id} not found in Order ${order?.id}`)

		if (item.stateInx <= OrderDetailStateEnum.NotProcessed)
		return;// skip

		//set new order
		updateOrderPosition(id, --item.stateInx);
	}

	function cancelItemState(id:string) {
		const item = order?.details.find(i => i.id === id)
		if (item === null) {
			console.error(`Position ${id} not found in Order ${order?.id}`)
			return;
		}

		updateOrderPosition(id, OrderDetailStateEnum.Canceled);
	}

	function updateOrderPosition(id: string, stateInx: number) {			
		const stateText = getStateDescription(stateInx);

		//set new order
		setOrder(prevOrder => ({
								...prevOrder,
								details: prevOrder?.details.map(d =>
									d.id === id ? { ...d, stateText, stateInx }
												: d)
							   })
		); 
	}
	const updateOrderList = (orderId: string, state: number) => {
		console.log('updateOrderList()');
		if (orderList?.find(o => o.id === orderId && o.state !== state))
		{
			console.log('updateOrderList()', "stateText has changed!");
			setOrderList(orderList?.map(o =>
				o.id === orderId ? {...o, state, stateText: state === 1? "отменен":  "готов"}
					             : o)
			);
		}
	};

	function getStateDescription(inx: number): string {
		//console.log(`${formatDateLong(new Date())} getStateDescription() states=[${inx}]`, states[inx])
		return states?.find(s => s.id === inx)?.description //(data as OrderDetailState[])
	}
	

	const openOrderList = () => setIsOpen(true)
	const closeOrderList = () => setIsOpen(false)

	function setCurrentOrder(order: Order | null) {
		setOrder(order);
		console.log('SetCurrentOrder', order)
	}

	return (
		<OrderListContext.Provider value={{ openOrderList, closeOrderList,
			order,
			orderList,
			curOrderId,
			setCurOrderId,
			setCurrentOrder,
			nextItemState,
			prevItemState,
			cancelItemState,
			getStateDescription,
			updateOrderList
		}}>
			{children}
			<OrderList isOpen={isOpen } />
		</OrderListContext.Provider>
	)
}