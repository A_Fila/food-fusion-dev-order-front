//users
export const getRouteUsers = '/admin/user';
export const getRouteGetUserList = '/auth/GetUsers';
export const getRouteUpdateUser = '/auth/UpdateUser';
export const getRouteDeleteUser = '/auth/DeleteUser';
export const getRouteCreateUser = '/auth/CreateUser';
//backed

//order
export const putOrderDetailsUrl = '/OrderDetails';
export const getOrderDetailStatesUrl = '/OrderDetailStates';
export const getOrderUrl = '/Orders';
export const getOrderListUrl = '/Orders/New';

//curier