export enum OrderDetailStateEnum {
    Canceled = 1,
    NotProcessed = 2,
    Cooking =3,
    Ready = 4
}