export enum OrderStateEnum {
    Canceled = 1,
    Open = 2,
    Paid = 3,
    Ready = 4,
    Delivered = 5
}