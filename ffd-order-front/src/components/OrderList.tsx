import { Offcanvas } from "react-bootstrap";
import {  useOrderListContext } from "../context/OrderListContext";
//import jsonOrdersList from '../data/ordersList.json'
import { AgGridReact } from "ag-grid-react";
import "ag-grid-community/styles/ag-grid.css"; // Mandatory CSS required by the grid
import "ag-grid-community/styles/ag-theme-quartz.css"; // Optional Theme applied to the grid
import { useCallback, useEffect, useState } from "react";
import { formatDate, formatDateLong } from "../utilities/formatDate";
import { formatCurrency } from "../utilities/formatCurrency";
import { ColDef, FirstDataRenderedEvent, GridReadyEvent, RowSelectedEvent, SelectionChangedEvent, } from "@ag-grid-community/core";
import './OrderList.css'

type OrderListProps = {
    isOpen: boolean
}

export function OrderList({ isOpen }: OrderListProps) {
    const { closeOrderList, orderList, curOrderId, setCurOrderId } = useOrderListContext();  

    const cellClassRules = {
        "agg-order-canceled": (params) => params.value === 'отменен',        
        "agg-order-open": (params) => params.value === 'открыт',
        "agg-order-ready": (params) => params.value === 'готов',       
    };
    const [columnDefs, setColumnDefs] = useState<ColDef[]>([
        //{ field: "id", headerName: "id", flex: 1, },    
        { field: "number", headerName: "№", flex: 1 },
        { field: "date", headerName: "дата", flex: 2, valueFormatter: (p: {value:Date}) => formatDate(new Date(p.value)) },
        {
            field: "stateText", headerName: "статус", flex: 2,
            cellClassRules
            //cellStyle: params => params.value === 'открыт' ? { background: 'blue', color: 'red' } : { color: 'black' }
        },
        { field: "total", headerName: "итого", flex: 1, valueFormatter: (p: { value: number }) => formatCurrency(p.value) },
          
    ]);

    const onRowSelected = async (event: RowSelectedEvent) => {        
        //console.log('onRowSelected()', `row #${event.node.data.number} selected = ${event.node.isSelected()}`);  //window.alert(msg);
        if (!event.node.isSelected())
            return;

        if (curOrderId !== event.node.data.id) {
            console.log('setCurOrderId=', event.node.data.id);
            setCurOrderId(event.node.data.id);
        }
    }

    //const onSelectionChanged = useCallback(
    //    (event: SelectionChangedEvent) => {
    //        //console.log('onSelectionChanged()', event)
    //        //console.log(event.api.getSelectedRows().values);
    //        //var rowCount = event.api.getSelectedNodes().length;
    //        //window.alert("onSelectionChanged(): " + rowCount + " rows selected");
    //    },
    //    [window],
    //);

    //const onGridReady = useCallback((params: GridReadyEvent) => {     
    //    //console.log("Grid is ready: locStorId=", curOrderId);

    //    //restore selected table row from LocalStorage
    //    //params.api.forEachNode(node => 
    //    //    node.data.id === node.setSelected(currentOrderId === node.data.id)
    //    //)
    //}, [curOrderId]);

    const onFirstDataRendered = (event: FirstDataRenderedEvent) => {
        console.log("AgGridReact: First Data Rendered");
        //restore selected table row from LocalStorage
        event.api.forEachNode(node =>
            node.data.id === node.setSelected(curOrderId === node.data.id)
        )
    }

    return (
        <Offcanvas show={isOpen} onHide={closeOrderList}
            placement="end" className="w-25">
            <Offcanvas.Header closeButton>
                <Offcanvas.Title>Список заказов</Offcanvas.Title>
            </Offcanvas.Header>

            <Offcanvas.Body>               
                <div
                    className="ag-theme-quartz" // applying the grid theme
                    style={{ height: "100%" }} // the grid will fill the size of the parent container
                >
                    <AgGridReact
                        rowData={orderList}
                        columnDefs={columnDefs}
                        rowSelection="single"
                        onRowSelected={onRowSelected}
                        //onSelectionChanged={onSelectionChanged}
                        //onGridReady={onGridReady}
                        onFirstDataRendered={onFirstDataRendered}
                    />
                </div>                
            </Offcanvas.Body>
        </Offcanvas>
    )
}