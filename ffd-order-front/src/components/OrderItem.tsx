import { Badge, Button, Card, OverlayTrigger, Tooltip } from "react-bootstrap";
import { formatCurrency } from "../utilities/formatCurrency";
import { useOrderListContext } from "../context/OrderListContext";
import { OrderDetailStateEnum } from "../enums/orderDetailStateEnum";
import { putOrderDetailsUrl } from "../constant/router";
import { useApi } from "../services/caller";

interface OrderItemProps {
    orderId: string;
    id: string;
    name: string;
    price: number;    
    count: number;
    stateInx: number;
    stateText: string
    description: string;
}
type EditOrderDetailRequest = {
    OrderId: string;
    DishId: string;
    State: number;
}

export function OrderItem(item: OrderItemProps) {
    const { nextItemState, prevItemState, cancelItemState, getStateDescription } = useOrderListContext() //itemStateError, itemStateLoading, itemStateSuccess    
    const { data, error, success, loading, fetchData: detailsPut } = useApi<EditOrderDetailRequest>(putOrderDetailsUrl, 'PUT');

    const nextStateText = getStateDescription(item.stateInx + 1)
    const prevStateText = getStateDescription(item.stateInx - 1)

    const bgClass = (function () {
        switch (item.stateInx) {
            case OrderDetailStateEnum.Canceled: return "danger";
            case OrderDetailStateEnum.NotProcessed: return "secondary";
            case OrderDetailStateEnum.Cooking: return "warning"
            case OrderDetailStateEnum.Ready: return "success";
        }
    })();

    const nextBtnHandler = () => {
        detailsPut({ "orderId": item.orderId, "dishId": item.id, "state": item.stateInx + 1 }); //call api
        nextItemState(item.id); //change state
    }
    const prevBtnHandler = () => {
        detailsPut({ "orderId": item.orderId, "dishId": item.id, "state": item.stateInx - 1 }); //call api
        prevItemState(item.id); //change state
    }

    const cancelBtnHandler = () => { //event: React.MouseEvent<HTMLButtonElement>
        detailsPut({ "orderId": item.orderId, "dishId": item.id, "state": OrderDetailStateEnum.Canceled }); //call api
        cancelItemState(item.id); //change state
    }

    //console.log('bgClass=',bgClass)
    return <>

        <Card className="h-100">            
            <Card.Img variant="top" src={'/images/' + item.id + '.jpeg'} height="250px"
                style={{ objectFit: "cover" }} />
            
            <Card.Body className="d-flex flex-column">
                <Card.Title className="d-flex justify-content-between align-items-baseline mb-4">
                    <span className="fs-2">{item.name}</span>
                    <span className="ms-2 text-muted">{formatCurrency(item.price)}</span>                    
                </Card.Title>
                
                <div className="fs-4">кол-во:&nbsp;
                    <Badge className="bg-success rounded-pill"> {item.count}</Badge>
                </div>
                <span className="fs-6 text-muted">id={item.id}</span>

                {error && <>
                            <p style={{ color: 'red' }}> Ошибка: {error.response?.data}</p>
                            <p>{JSON.stringify(error)})</p>
                </>}

                <div className="mt-auto">
                    <div className="d-flex align-items-center flex-column" style={{ gap: ".5rem" }}>
                        <div className="fs-5 text-muted">статус позиции:</div>
                        <div className="d-flex align-items-center justify-content-center" style={{ gap: ".5rem" }}>

                            {/*<OverlayTrigger placement="top" overlay={<Tooltip>предыдущее состояние</Tooltip>}>*/}
                            {item.stateInx > OrderDetailStateEnum.NotProcessed ?
                                (<Button title={prevStateText} onClick={prevBtnHandler} data-bs-toggle="tooltip">
                                        &lt;
                                 </Button>)                                : (<></>)
                            }   {/*</OverlayTrigger>*/ }

                           
                            <div>
                               
                                <Badge pill className="fs-4" bg={bgClass} >
                                    {loading && <div>Загрузка...</div>}
                                    {!loading && !error && <div>{item.stateText}({item.stateInx})</div>}
                                </Badge>
                              
                               
                            </div>
                            {/*<OverlayTrigger placement="top" overlay={<Tooltip>следующее состояние</Tooltip>}> */}
                            {item.stateInx < OrderDetailStateEnum.Ready ?
                                (<Button title={nextStateText} onClick={nextBtnHandler}>
                                        &gt;
                                 </Button>)                          : (<></>) 
                            }
                            {/*</OverlayTrigger>*/}

                        </div>
                        {item.stateInx !== OrderDetailStateEnum.Canceled ?
                            (<Button variant="danger" size="sm" title="отменить позицию" onClick={cancelBtnHandler}>
                                отменить
                            </Button>)                              : <></>
                        }
                    </div>
                </div>
            </Card.Body>

        </Card>
        <br />
    </>
}