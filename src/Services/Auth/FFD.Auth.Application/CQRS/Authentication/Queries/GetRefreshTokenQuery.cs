﻿using FFD.Auth.Application.Exceptions;
using FFD.Auth.Core.Entities.Db;
using FFD.Auth.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using FluentValidation;
using FFD.Auth.Core.IServices;
using FFD.Auth.Core.User;

namespace FFD.Auth.Application.CQRS.Authentication.Queries
{
    public class GetRefreshTokenQuery : IRequest<TokenDto>
    {
        public string Login { get; set; }
        public string RefreshToken { get; set; }        
    }

    public class GetRefreshTokenQueryHandler : IRequestHandler<GetRefreshTokenQuery, TokenDto>
    {
        //private readonly SignInManager<User> _signInManager;
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;      

        public GetRefreshTokenQueryHandler(ITokenGenerator tokenGenerator, IDbContext dbContext, IMapper mapper)
        {
            //_signInManager = signInManager;
            _tokenGenerator = tokenGenerator;
            _dbContext = dbContext;
            _mapper = mapper;           
        }

        public async Task<TokenDto> Handle(GetRefreshTokenQuery request, CancellationToken cancellationToken)
        {
            Core.Entities.Db.User user;
                user = _dbContext.Users.FirstOrDefault(x => x.Email.ToLower() == request.Login.ToLower().ToLower());
            if (user == null)
            {
                throw new NotAuthorizedException();
            }
            if (!user.RefreshToken.Equals(request.RefreshToken))
            {
                throw new NotAuthorizedException("Refresh Token Invalid");
            }
            else if (user.TokenExpires < DateTime.Now)
            {
                throw new NotAuthorizedException("Token expired");
            }
            var userClaims = _tokenGenerator.GenerateClaims(user);
            var refreshToken = _tokenGenerator.GenerateRefreshToken();
            _mapper.Map(refreshToken, user);
            await _dbContext.SaveChangesAsync();
            var token = _tokenGenerator.Generate(userClaims);
            return new TokenDto(user.Id, token, refreshToken.Token, user.Login);
        }
    }

    public sealed class GetRefreshTokenQueryValidator : AbstractValidator<GetRefreshTokenQuery>
    {
        public GetRefreshTokenQueryValidator()
        {
            //RuleFor(x => x.Email).EmailAddress();
            //RuleFor(x => x.Password).NotEmpty();
        }
    }
}
