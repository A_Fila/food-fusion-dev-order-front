﻿using FFD.Auth.Application.Exceptions;
using FFD.Auth.Core.Entities.Db;
using FFD.Auth.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using FluentValidation;
using FFD.Auth.Core.User;
using FFD.Auth.Core.IServices;

namespace FFD.Auth.Application.CQRS.Authentication.Queries
{
    public class GetAccessTokenQuery : IRequest<TokenDto>
    {
        public string Login { get; set; }
        public string Password { get; set; }       
    }

    public class GetAccessTokenQueryHandler : IRequestHandler<GetAccessTokenQuery, TokenDto>
    {
        //private readonly SignInManager<User> _signInManager;
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;

        public GetAccessTokenQueryHandler(ITokenGenerator tokenGenerator, IDbContext dbContext, IMapper mapper)
        {
            //_signInManager = signInManager;
            _tokenGenerator = tokenGenerator;
            _dbContext = dbContext;
            _mapper = mapper;         
        }

        public async Task<TokenDto> Handle(GetAccessTokenQuery request, CancellationToken cancellationToken)
        {
            FFD.Auth.Core.Entities.Db.User user;
            var limitFailureLogin = await _dbContext.Settings.FirstOrDefaultAsync(x => x.Key.ToLower() == "LimitFailureLogin".ToLower());
            user = _dbContext.Users.FirstOrDefault(x => x.Login.ToLower() == request.Login.ToLower());
            if (user == null && request.Login.Contains("@"))
                user = _dbContext.Users.FirstOrDefault(x => x.Email.ToLower() == request.Login.ToLower());
            if (user == null)
            {
                throw new NotAuthorizedException();
            }
            var checkPasswordResult = _tokenGenerator.VerifyPasswordHash(request.Password, user.PasswordHash, user.PasswordSalt);
            if (!checkPasswordResult)
            {
                user.LoginFailures += 1;
                if (byte.TryParse(limitFailureLogin?.Value, out byte limitFailureLoginValue))
                    if (user.LoginFailures >= limitFailureLoginValue)
                    {
                        user.Status = "Blocked";
                        await _dbContext.SaveChangesAsync();
                        throw new UserBlockedException();
                    }
                await _dbContext.SaveChangesAsync();
                throw new NotAuthorizedException();
            }
            user.LoginFailures = 0;
            var userClaims = _tokenGenerator.GenerateClaims(user);
            RefreshToken? refreshToken = _tokenGenerator.GenerateRefreshToken();
            try
            {

                _mapper.Map(refreshToken, user);
                await _dbContext.SaveChangesAsync();

                var token = _tokenGenerator.Generate(userClaims);
                return new TokenDto(user.Id, token, refreshToken.Token, user.Login);//, user.Picture);//user.Id,
            }
            catch (Exception ex)
            {
                var tt = 1;
            }
            return null;
        }
    }

    public sealed class GetAccessTokenQueryValidator : AbstractValidator<GetAccessTokenQuery>
    {
        public GetAccessTokenQueryValidator()
        {
            //RuleFor(x => x.Email).EmailAddress();
            RuleFor(x => x.Password).NotEmpty();
        }
    }
}
