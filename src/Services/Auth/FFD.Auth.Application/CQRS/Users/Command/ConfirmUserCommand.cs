﻿using AutoMapper;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using Microsoft.EntityFrameworkCore;
using FFD.Auth.Core;
using FFD.Auth.Core.IServices;
using FFD.Auth.Application.Exceptions;
using FFD.Auth.Infrastructure.Services;

namespace FFD.Auth.Application.CQRS.User.Commands
{
    public class ConfirmUserCommand : IRequest<bool>
    {
        public string? Email { get; set; }
        public string? Code { get; set; }
    }

    public class ConfirmUserCommandHandler : IRequestHandler<ConfirmUserCommand, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IEmailService _emailService;

        public ConfirmUserCommandHandler(IDbContext dbContext, IMapper mapper, ITokenGenerator tokenGenerator, IEmailService emailService)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _tokenGenerator = tokenGenerator;
            _emailService = emailService;
        }

        public async Task<bool> Handle(ConfirmUserCommand request, CancellationToken cancellationToken)
        {          
            var user = _dbContext.Users.FirstOrDefault(x => x.Email == request.Email);
            if (user == null)
            {
                throw new NotFoundException();
            }
            if (user.EmailConfirmationToken == request.Code)
            {
                user.ConfirmEmail = true;
                await _dbContext.SaveChangesAsync();
            }            
            return true;
        }
    }
    public sealed class ConfirmUserCommandValidator : AbstractValidator<ConfirmUserCommand>
    {
        public ConfirmUserCommandValidator()
        {
            RuleFor(x => x.Email).NotEmpty();
        }
    }
}
