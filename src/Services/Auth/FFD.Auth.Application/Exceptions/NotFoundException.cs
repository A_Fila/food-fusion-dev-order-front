﻿namespace FFD.Auth.Application.Exceptions
{
    public class NotFoundException : ApplicationException
    {
        public NotFoundException(string message = "Not found") : base(message)
        {
        }
    }
}
