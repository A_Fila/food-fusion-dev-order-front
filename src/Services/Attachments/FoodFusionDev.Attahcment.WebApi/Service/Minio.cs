﻿using FoodFusionDev.Attahcment.WebApi.Controllers;
using FoodFusionDev.Attahcment.WebApi.Model;
using Minio;
using Minio.DataModel.Args;

namespace FoodFusionDev.Attahcment.WebApi.Service
{
    public class Minio
    {
        private readonly IMinioClient _minioClient;
        private readonly ILogger<Minio> _logger;
        public Minio(ILogger<Minio> logger, IMinioClient minioClient)
        { 
            _minioClient = minioClient;
            _logger = logger;
        }
        public async Task<string> PutObj(PutObjectRequest request)
        {

            var bucketName = request.bucket;
            // Check Exists bucket
            bool found = await _minioClient.BucketExistsAsync(new BucketExistsArgs().WithBucket(bucketName));
            if (!found)
            {
                // if bucket not Exists,make bucket
                await _minioClient.MakeBucketAsync(new MakeBucketArgs().WithBucket(bucketName));
            }

            System.IO.MemoryStream filestream = new System.IO.MemoryStream(request.data);

            var filename = Guid.NewGuid();
            // upload object
            await _minioClient.PutObjectAsync(new PutObjectArgs()
                .WithBucket(bucketName).WithFileName(filename.ToString())
                .WithStreamData(filestream).WithObjectSize(filestream.Length)
                );

            return await Task.FromResult<string>(filename.ToString());
        }
        public async Task<GetObjectReply> GetObject(string bucket, string objectname)
        {


            MemoryStream destination = new MemoryStream();
            // Check Exists object
            var objstatreply = await _minioClient.StatObjectAsync(new StatObjectArgs()
                                          .WithBucket(bucket)
                                          .WithObject(objectname)
                                          );

            if (objstatreply == null || objstatreply.DeleteMarker)
                throw new Exception("object not found or Deleted");

            // Get object
            await _minioClient.GetObjectAsync(new GetObjectArgs()
                                        .WithBucket(bucket)
                                        .WithObject(objectname)
                                        .WithCallbackStream((stream) =>
                                        {
                                            stream.CopyTo(destination);
                                        }
                                        )
                                       );

            return await Task.FromResult<GetObjectReply>(new GetObjectReply()
            {
                data = destination.ToArray(),
                objectstat = objstatreply

            });
        }
    }
}
