﻿using FDD.Shared.Interfases;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using System.Security.Claims;

namespace FFD.Chat.Services
{
    public class UserAccessorSignalR 
    {
        public UserAccessorSignalR(ClaimsPrincipal user)
        {
            User = user;
        }

        public ClaimsPrincipal User; //=> _accessor.HttpContext.User;
        public Guid? Id => User.FindFirstValue("Id") == null ? Guid.Empty : new Guid(User.FindFirstValue("Id"));
        public string? Role => User.FindFirstValue("Role") == null ? "" : User.FindFirstValue("Role");
        public string? FirstName => User.FindFirstValue("FirstName") == null ? "" : User.FindFirstValue("FirstName");
        public string? LastName => User.FindFirstValue("LastName") == null ? "" : User.FindFirstValue("LastName");
        public string? MiddleName => User.FindFirstValue("MiddleName") == null ? "" : User.FindFirstValue("MiddleName");
        public string? Email => User.FindFirstValue("Email") == null ? "" : User.FindFirstValue("Email");
        public string? Login => User.FindFirstValue("Login") == null ? "" : User.FindFirstValue("Login");
    }
    public class UserAccessorSignalR2 : IUserIdProvider
    {
        //private HubCallerContext _accessor;

        public string GetUserId(HubConnectionContext connection)
        {
            var User = connection.User.Identity;
            return "fake-user-id";

        }
        public ClaimsPrincipal User;//=> _accessor.User;
        //public Guid? Id => User.FindFirstValue("Id") == null ? Guid.Empty : new Guid(User.FindFirstValue("Id"));
        //public string? Role => User.FindFirstValue("Role") == null ? "" : User.FindFirstValue("Role");
        //public string? FirstName => User.FindFirstValue("FirstName") == null ? "" : User.FindFirstValue("FirstName");
        //public string? LastName => User.FindFirstValue("LastName") == null ? "" : User.FindFirstValue("LastName");
        //public string? MiddleName => User.FindFirstValue("MiddleName") == null ? "" : User.FindFirstValue("MiddleName");
        //public string? Email => User.FindFirstValue("Email") == null ? "" : User.FindFirstValue("Email");
        //public string? Login => User.FindFirstValue("Login") == null ? "" : User.FindFirstValue("Login");
    }
}
