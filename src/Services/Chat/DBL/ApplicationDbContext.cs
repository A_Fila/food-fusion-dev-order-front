﻿
using FDD.Shared.Interfases;
using Microsoft.EntityFrameworkCore;
using FFD.Chat.DBL.Model;
namespace FFD.Chat.DBL
{
    public class ApplicationDbContext : DbContext, IDbContext
    {
        //private readonly IUserAccessor _userAccessor;
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IUserAccessor userAccessor = null) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            //_userAccessor = userAccessor;
            //Database.EnsureCreated();
        }
        public DbSet<Order> Orders { get; set; }
        public DbSet<ChatModel> Chats { get; set; }
    }
}
