﻿using FFD.Chat.DBL.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Reflection.Metadata;
namespace FFD.Chat.DBL
{
    public interface IDbContext
    {
        DbSet<FFD.Chat.DBL.Model.ChatModel> Chats { get; set; }
        DbSet<Order> Orders { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        EntityEntry<T> Entry<T>(T entity) where T : class;
    }
}