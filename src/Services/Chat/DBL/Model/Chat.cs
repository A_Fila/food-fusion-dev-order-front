﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Chat.DBL.Model
{
    public partial class ChatModel
    {
        public int Id { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? CourierId { get; set; }
        public string Message { get; set; }
        public DateTime DataRecord { get; set; }
        public long OrderId { get; set; }
        public string? Attachment { get; set; }

    }
}
