﻿using FFD.OrderService.Core.Entities;
using System.ComponentModel.DataAnnotations;

public class OrderDetailState: BaseEntity
{
    [Key]
    public int StateId {  get; set; }                                                                    
    public string Name { get; set; }                
    public string Description { get; set; }   
}