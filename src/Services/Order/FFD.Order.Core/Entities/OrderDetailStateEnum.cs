﻿using System.ComponentModel;

namespace FFD.OrderService.Core.Entities
{
    /// <summary>
    /// статус позиции заказа
    /// </summary>
    public enum OrderDetailStateEnum //: BaseEntity
    {
        [Description("отменена")]
        Canceled = 1,
        
        [Description("не обработана")]
        NotProcessed = 2,

        [Description("готовится...")]
        Cooking = 3,

        [Description("готова")]
        Ready = 4
    }
}