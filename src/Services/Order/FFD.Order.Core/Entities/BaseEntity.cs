﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.OrderService.Core.Entities
{
    
    public class BaseEntity 
    {
        [StringLength(36, ErrorMessage = "{0} length must be equal {1}.", MinimumLength = 36)]
        public Guid Id { get; set; }
    }
}
