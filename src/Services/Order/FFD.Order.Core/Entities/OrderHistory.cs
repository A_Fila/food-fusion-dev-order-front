﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.OrderService.Core.Entities
{
    public class OrderHistory : BaseEntity
    {
        public Order Order { get; set; } // Required reference navigation to principal
        public Guid OrderId  { get; set; } // Required foreign key property
        public DateTime StateDate { get; set; }
        public OrderStateEnum State { get; set; }
    } 
}
