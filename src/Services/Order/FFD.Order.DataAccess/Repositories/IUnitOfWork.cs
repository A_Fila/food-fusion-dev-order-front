﻿using FFD.OrderService.Core.Entities;

namespace FFD.OrderService.DataAccess.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        Context Context { get; }
        IRepository<T> GetRepository<T>() where T : BaseEntity;

        /// <summary>
        /// Сохранить изменения асинхронно
        /// </summary>
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);        
    }
}