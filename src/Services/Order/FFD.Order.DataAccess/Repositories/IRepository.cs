﻿using FFD.OrderService.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;


namespace FFD.OrderService.DataAccess.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(params object[] Ids);

        Task<T> AddAsync(T element);
        Task AddRangeAsync(T[] elements);

        Task<int> DeleteByIdAsync(Guid id);
        Task<int> DeleteAsync(T entity);

        Task<T> GetByIdAsync(Expression<Func<T, bool>> filter, string[] navProps);

        IQueryable<T> FindAllBy(Expression<Func<T, bool>> predicate);     //IQueryable<T> FindBy(Expression<Func<T, bool>> expression) => DatabaseContext.Set<T>().Where(expression).AsNoTracking();
        Task<List<T>> FindAllByAsync(Expression<Func<T, bool>> predicate);


        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);

        int SaveChanges();
    }
}