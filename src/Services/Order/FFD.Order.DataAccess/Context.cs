﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Reflection.Metadata;
using FFD.OrderService.Core.Entities;
using FFD.OrderService.DataAccess.Data;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace FFD.OrderService.DataAccess
{
    public class Context : DbContext
    {
        //ctor
        //public Context() 
        //{
        //    Database.EnsureDeleted();
        //    Database.EnsureCreated();
        //}
        public Context(DbContextOptions<Context> options) : base(options)
        {
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    //get connection string
        //     //var strConn = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;
        //    var strConn = ConfigurationManager.ConnectionStrings["Default"].ConnectionString;

        //    optionsBuilder.UseNpgsql(strConn)
        //        .LogTo(Console.WriteLine, Microsoft.Extensions.Logging.LogLevel.Information)
        //        .EnableSensitiveDataLogging();                        
        //}

        protected override void ConfigureConventions(ModelConfigurationBuilder configurationBuilder)
        {
            base.ConfigureConventions(configurationBuilder);

            configurationBuilder.Properties<string>()
                .HaveMaxLength(1024);//255
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Orders
            modelBuilder.Entity<Order>()
                .Property(p => p.State)
                .HasConversion<int>();

            //modelBuilder.Entity<Order>()
            //    .HasOne<OrderState>()
            //    .WithMany()
            //    .HasForeignKey(o => o.State)
            //    .HasPrincipalKey(s => s.Id);//(e => (int)e.State);

            modelBuilder.Entity<Order>().HasData(FakeDataFactory.Orders);

            //Clients
            modelBuilder.Entity<Client>().HasIndex(p => p.PhoneNumber).IsUnique();
            modelBuilder.Entity<Client>().HasIndex(p => p.Email).IsUnique();
            modelBuilder.Entity<Client>().HasData(FakeDataFactory.Clients);

            //OrderHistories 
            modelBuilder.Entity<OrderHistory>().HasKey(e => new { e.OrderId, e.State }); //add composite primary key
            modelBuilder.Entity<OrderHistory>().Ignore(c => c.Id);                       //delete Id key (extends from BaseEntity))
            modelBuilder.Entity<OrderHistory>().HasData(FakeDataFactory.OrderHistories);

            //OrderDetails 
            modelBuilder.Entity<OrderDetail>().HasKey(e => new { e.OrderId, e.DishId }); //add composite primary key
            //modelBuilder.Entity<OrderDetail>().HasKey(e => new { e.DId, e.})
            modelBuilder.Entity<OrderDetail>().Ignore(c => c.Id);                        //delete Id key (extends from BaseEntity))
            modelBuilder.Entity<OrderDetail>().HasData(FakeDataFactory.OrderDetails);

            //Dishes  
            modelBuilder.Entity<Dish>().HasData(FakeDataFactory.Dishes);

            //OrderState
            modelBuilder.Entity<OrderState>().HasKey(e => e.Id);
            //modelBuilder.Entity<OrderState>()
            //    .HasMany<Order>()
            //    .WithOne()
            //    .HasForeignKey(e => (int)e.State);
                

            modelBuilder.Entity<OrderState>().HasData(
                            Enum.GetValues(typeof(OrderStateEnum))
                                .Cast<OrderStateEnum>()
                                .Select(e => new OrderState { 
                                                   Id = (int)e, 
                                                   Name = e.ToString(), 
                                                   Description = e.GetDescription() 
                                                  }
                                )
            );
            //OrderDetailState
            modelBuilder.Entity<OrderDetailState>().HasKey(e => e.StateId);
            modelBuilder.Entity<OrderDetailState>().Ignore(p => p.Id);
            modelBuilder.Entity<OrderDetailState>().HasData(
                            Enum.GetValues(typeof(OrderDetailStateEnum))
                                .Cast<OrderDetailStateEnum>()
                                .Select(e => new OrderDetailState
                                {
                                    StateId = (int)e,
                                    Name = e.ToString(),
                                    Description = e.GetDescription()
                                }
                                )
            );
        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<OrderHistory> OrderHistories { get; set; }
        //public DbSet<Waiter> Waiters { get; set; }

        public DbSet<Client> Clients { get; set; }
        // public DbSet<Table> Tables { get; set; }
        //public DbSet<Table> Menus { get; set; }
        public DbSet<Dish> Dishes { get; set; }

        public DbSet<OrderState> OrderStates {get;set;}
        public DbSet<OrderDetailState> OrderDetailStates { get; set; }
    }
}
