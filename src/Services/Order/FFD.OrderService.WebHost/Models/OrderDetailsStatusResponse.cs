﻿namespace FFD.OrderService.WebHost.Models
{
    public class OrderDetailsStatusResponse
    {
        public int Id {  get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
