﻿using FFD.OrderService.Core.Entities;

namespace FFD.OrderService.WebHost.Models
{
    public class EditOrderRequest
    {
        //Order
        public Guid Id { get; set; }
       // public string Number { get; set; }

 //       public DateTime Date { get; set; }

        public int State { get; set; }

        public double Total { get; set; }

        public Guid ClientId { get; set; }
    }
}
