﻿namespace FFD.OrderService.WebHost.Models
{
    public class OrderResponse
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public double Total {  get; set; }
        public string Status { get; set; }
        public string StatusText { get; set; }
        public List<OrderDetailsResponse> Details { get; set; } = [];
    }
}
