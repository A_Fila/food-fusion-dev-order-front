﻿namespace FFD.OrderService.WebHost.Models
{
    public class AddOrderDetailRequest
    {
        public Guid OrderId { get; set; } // Required foreign key property
        public Guid DishId { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public int Count { get; set; }
    }
}