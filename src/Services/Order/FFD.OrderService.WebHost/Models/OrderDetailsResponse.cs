﻿namespace FFD.OrderService.WebHost.Models
{
    public class OrderDetailsResponse
    {
        public Guid Id {  get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
        public ulong Count { get; set; }
        public string StateText { get; set; }
        public int StateInx { get; set; }
    }
}
