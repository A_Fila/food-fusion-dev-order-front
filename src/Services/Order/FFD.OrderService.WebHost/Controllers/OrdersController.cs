﻿using FDD.Bus.Models;
using FDD.Shared.Interfases;
using FFD.OrderService.Core.Entities;
using FFD.OrderService.DataAccess.Repositories;
using FFD.OrderService.WebHost.Logic;
using FFD.OrderService.WebHost.Models;
using FFD.OrderService.WebHost.Notifier;
using FFD.Shared.Models;
using MassTransit;
using MassTransit.Initializers;
using Microsoft.AspNetCore.Mvc;


namespace FFD.OrderService.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrdersController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        private readonly IRepository<Order> _orderRepo;
        private readonly IRepository<OrderHistory> _historyRepo;

        private readonly IPublishEndpoint _publishEndpoint;

        private readonly OrderHelper _orderHelper;
        private readonly OrderNotifier _notifier;
        private readonly IUserAccessor _userAccessor;

        public OrdersController(IUnitOfWork unitOfWork,
                                //IPublishEndpoint publishEndpoint,
                                OrderNotifier notifier,
                                IUserAccessor userAccessor )
        {
            _unitOfWork = unitOfWork;
            _orderHelper = new OrderHelper(_unitOfWork);
            _orderRepo = _unitOfWork.GetRepository<Order>();
            
            _historyRepo = _unitOfWork.GetRepository<OrderHistory>();

            //_publishEndpoint = publishEndpoint;
            _notifier = notifier;
            _userAccessor = userAccessor;
        }

        /// <summary>
        /// Тестовая отправка позиции заказа
        /// </summary>
        /// <param name="request">Данные запроса </param>
        /// <returns></returns>
        //[HttpPost("PublishToBroker")]
        //public async Task<ActionResult> PublishOrder(BusCreateOrder request)
        //{
        //    //публикация сообщения с помощью MassTransit
        //    //await _publishEndpoint.Publish<BusCreateOrder>(request);

        //    //_notifier.NotifyMessage(null);

        //    return Ok();
        //}

        /// <summary>
        /// Создание нового заказа
        /// </summary>
        /// <param name="request">Данные запроса:</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateOrderAsync(
            CreateOrderRequest request)
        {
            var orderDtl = request.OrderDetails.Select(d =>
                    new OrderDetail()
                    {
                        DishId = d.DishId,
                        Price = d.Price,
                        Discount = d.Discount,
                        Count = d.Count
                    })
                .ToArray();

            OrderHelper orderHelper = new OrderHelper(_unitOfWork);
            Order order;
            try
            {
                order = await orderHelper.CreateAsync(request.ClientId, orderDtl);
            }
            catch (Exception ex)
            {
                return BadRequest( ex.Message );
            }
            #region old version
            //var client = await _clientRepository.GetByIdAsync(request.ClientId);

            //if (client == null)
            //    return NotFound($"Client with id={request.ClientId} not found");

            //var order = new Order()
            //{
            //    ClientId = request.ClientId,
            //    Date = DateTime.Now.ToUniversalTime(),
            //    State = OrderStateType.Open,
            //    StateDate = DateTime.Now.ToUniversalTime(),
            //    //Total = request.Total,
            //};

            //using (var readTransaction = await _context.Database.BeginTransactionAsync(IsolationLevel.Serializable))
            //{
            //    order.Number = GetOrderNumber(order.Date.Year);

            //    await _orderRepository.AddAsync(order);

            //    await readTransaction.CommitAsync(); //commit
            //}

            //await AddOrderHistoryAsync(order);
            #endregion

            return CreatedAtAction(nameof(GetOrderByIdAsync), new { id = order.Id }, order); //order
        }

        /// <summary>
        /// Редактирование заказа
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> EditOrderAsync(EditOrderRequest request)
        {
            if (!Enum.IsDefined(typeof(OrderStateEnum), request.State))// !Enum.TryParse<OrderStateType>(request.State.ToString(), out var state)
                return BadRequest($"Unknown order state: {request.State}");

            var order = await _orderRepo.GetByIdAsync(request.Id); //_orderRepository

            if (order == null)
                return NotFound($"Order id={request.Id} not found");

            order.Total = request.Total;
            order.State = (OrderStateEnum)request.State ;//Enum.Parse<OrderStateEnum>( 

            await _orderHelper.AddOrderHistoryAsync(order); //AddOrderHistoryAsync(order);

            await _unitOfWork.SaveChangesAsync();//_orderRepository

            return Ok();
        }


        /// <summary>
        /// Получение списка всех заказов
        /// </summary>
        /// <returns>список заказов</returns>
        [HttpGet]
        public async Task<IEnumerable<OrderShortResponse>> GetAllOrdersAsync()
        {
            var orders = await _orderRepo.GetAllAsync();

            return orders.Select(o => new OrderShortResponse
            {
                Id = o.Id,
                Number = o.Number,
                Date = o.Date,
                Total = o.Total,
                State = o.State,
                StateText = o.State.GetDescription()
            });
        }

        /// <summary>
        /// Получение списка необработанных(новых) заказов
        /// </summary>
        /// <returns>список заказов</returns>
        [HttpGet]
        [Route("New")]
        public async Task<IEnumerable<OrderShortResponse>> GetNewOrdersAsync()
        {
            var orders = await _orderRepo.FindAllByAsync(o => o.State == OrderStateEnum.Open);

            return orders.Select(o => new OrderShortResponse
            {
                Id = o.Id,
                Number = o.Number,
                Date = o.Date,
                Total = o.Total,
                State = o.State,
                StateText = o.State.GetDescription()
            }).OrderByDescending(o => o.Date);
        }

        /// <summary>
        /// Получить заказ по id
        /// </summary>
        /// <param name="id" example="44328385-ddbf-4e78-8b51-c9739e67078d"> идентификатор заказа </param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<OrderResponse>> GetOrderByIdAsync(Guid id)//Order
        {
            var order = await _orderRepo.GetByIdAsync(o => o.Id == id, ["Client", "OrderHistories", "OrderDetails.Dish" ]);//OrderHistories

            if (order == null)
                return NotFound($"Order id={id} not found");

            var response = new OrderResponse()
            {
                Id = order.Id,
                Date = order.Date,
                Number = order.Number,
                Total = order.Total,
                Status = order.State.ToString(),
                StatusText = order.State.GetDescription(),
                Details = order.OrderDetails.Select(d => new OrderDetailsResponse()
                {
                    Id = d.Dish.Id,
                    Name = d.Dish.Name,
                    Description = d.Dish.Description,
                    Count = d.Count,
                    Price = d.Price,
                    StateText = d.State.GetDescription(),
                    StateInx = (int)d.State
                }).ToList()
            };

            return response;
        }

        /// <summary>
        /// Удаление заказа
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteOrderAsync(Guid id)
        {
            int recAff = await _orderRepo.DeleteByIdAsync(id);

            return (recAff == 0)? NotFound($"Order id={id} not found")
                                : Ok($"Order id={id} has been deleted");           
        }



        #region OrderHistory CRUD
        ///// <summary>
        ///// Добавление записи изменения статуса заказа
        ///// </summary>
        ///// <param name="request"></param>
        ///// <returns></returns>
        //private async Task<OrderHistory> AddOrderHistoryAsync(Order order)
        //{
        //    var orderHistory = new OrderHistory()
        //    {
        //        OrderId = order.Id,
        //        State = order.State,
        //        StateDate = order.StateDate
        //    };

        //    await _historyRepo.AddAsync(orderHistory); //_historyRepository

        //    return orderHistory;
        //}

        /// <summary>
        /// Получение списка изменений по заказу
        /// </summary>
        /// <returns></returns>
        //private async Task<IEnumerable<OrderHistory>> GetOrderHistoryAsync(Guid orderId)//ActionResult Task<List<Table>>
        //{
        //    return await _ohRepository.FindAllByAsync(h => h.OrderId == orderId);
        //}
        #endregion
    }
}