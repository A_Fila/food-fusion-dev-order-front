﻿using FFD.OrderService.DataAccess.Repositories;
using FFD.OrderService.WebHost.Models;
using Microsoft.AspNetCore.Mvc;

namespace FFD.OrderService.WebHost.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderDetailStatesController : ControllerBase
    {
        readonly IRepository<OrderDetailState> _stateRepository;

        public OrderDetailStatesController(IRepository<OrderDetailState> repository)
        {
            _stateRepository = repository;
        }

        /// <summary>
        /// Получение списка всех статусов
        /// </summary>
        /// <returns>список клиентов</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<OrderDetailsStatusResponse>>> GetAllStatesAsync()//ActionResult Task<List<Table>>
        {
            var states = await _stateRepository.GetAllAsync();
            if (states == null)
                return NotFound();

            var response = states.Select(s => new OrderDetailsStatusResponse
            {
                Id = s.StateId,
                Name = s.Name,
                Description = s.Description
            });
            return Ok(response);
        }

        /// <summary>
        /// Получить статус по id
        /// </summary>
        /// <param name="id" example="1">идентификатор статуса, например: <example>1</example></param>        
        /// <returns></returns>
        [HttpGet("{id:int}")]
        public async Task<ActionResult<OrderDetailsStatusResponse>> GetClientsByIdAsync(int id)
        {
            var state = await _stateRepository.GetByIdAsync(id);//o => o.Id == id, ["Client"]

            if (state == null)
                return NotFound();

            return new OrderDetailsStatusResponse(){
                Id = state.StateId,
                Name = state.Name,
                Description = state.Description
            };
        } 
    }
}