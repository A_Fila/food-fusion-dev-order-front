﻿using FDD.Bus.Models;
using FDD.Shared.Interfases;
using FFD.OrderService.Core.Entities;
using FFD.Shared.Models;
using FFD.Shared.RabbitMq;
using System.Diagnostics;
using System.Text.Json;


namespace FFD.OrderService.WebHost.Notifier
{
    public class OrderNotifier
    {
        private readonly QueueSender _queueSender;
        private readonly IUserAccessor _userAccessor;
        
        public OrderNotifier(QueueSender queueSender, 
                             IUserAccessor userAccessor )
        {
            _queueSender = queueSender;
            _userAccessor = userAccessor;            
        }

        public void NotifyMessage(OrderDetail item)
        {
            var message = new BusOrderItemStatusChanged//DishRabbit
            {
                OrderId = item.OrderId,
                DishId = item.DishId,
                Count  = item.Count,
                State =  (int)item.State,
                StateText = item.State.ToString(),

                User = new UserDto
                {
                    FirstName = _userAccessor.FirstName,
                    LastName = _userAccessor.LastName,
                    Id = _userAccessor.Id,
                    MiddleName = _userAccessor.MiddleName,
                    Login = _userAccessor.Login,
                    Email = _userAccessor.Email
                }
            };

            _queueSender.Send(JsonSerializer.Serialize(message), ["Order"]);
        }
    }
}