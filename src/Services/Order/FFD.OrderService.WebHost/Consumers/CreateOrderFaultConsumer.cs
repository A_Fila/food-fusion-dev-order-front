﻿using FDD.Bus.Models;
using MassTransit;

namespace FFD.OrderService.WebHost.Consumers
{
    public class CreateOrderFaultConsumer : IConsumer<Fault<BusCreateOrder>>
    {
        private readonly ILogger<CreateOrderFaultConsumer> _logger;
        public CreateOrderFaultConsumer(ILogger<CreateOrderFaultConsumer> logger)
        {
            _logger = logger;
        }
        public Task Consume(ConsumeContext<Fault<BusCreateOrder>> context)
        {
            var error = context.Message.Exceptions[0];
            _logger.LogError(error.Message);

            return Task.CompletedTask;
        }
    }
}