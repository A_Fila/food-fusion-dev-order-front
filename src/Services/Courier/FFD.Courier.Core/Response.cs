﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Core
{
    public class Response<T>
    {
        public int Result { get; set; }
        public string Message { get; set; }
        public T? Body { get; set; }

        private Response(int result = 200, string msg = "Success", T? body = default)
        {
            this.Result = result;
            this.Message = msg;
            this.Body = body;
        }
        public static Response<T> Success(T body)
        {
            return new Response<T>(body: body);
        }
        public static Response<T> Fail(int r, string m)
        {
            return new Response<T>(result: r, msg: m);
        }
    }
}
