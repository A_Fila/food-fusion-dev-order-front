﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Core.Entities.Dto
{
    public class MessageDto
    {
        public int Id { get; set; }
        public Guid? ClientId { get; set; }
        public Guid? CourierId { get; set; }
        public string Message { get; set; }
        public DateTime DataRecord { get; set; }
        public int OrderId { get; set; }
        public string? Attachment { get; set; }
        public string? Client { get; set; }
        public string? Courier { get; set; }
    }
}
