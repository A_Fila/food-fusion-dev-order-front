﻿using FDD.Shared.Interfases;
using FDD.Shared.Services;
using FFD.Courier.Core;
using FFD.Courier.Infrastructure;
namespace FFD.Courier.Api
{

    public static class DependencyInjection
    {
        public static void InjectDependencies(this WebApplicationBuilder builder)
        {
            builder.Services.AddScoped<IDbContext, ApplicationDbContext>();
            builder.Services.AddScoped<IUserAccessor, UserAccessor>();           
        }
    }
}
