﻿using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FFD.Shared.Models;
using FFD.Shared.RabbitMq;
using FFD.Shared.RabbitMq.Settings;
using System.Diagnostics;
using System.Text.Json;

namespace FFD.Courier.Api.Listener
{
    public class CourierQueueListener : QueueListener
    {    
        private readonly IServiceScopeFactory _scopeFactory;
        public CourierQueueListener(BrokerSettings brokerSettings, ReceiverSettings receiverSettings, IServiceScopeFactory scopeFactory) : base(brokerSettings, receiverSettings)
        {            
            _scopeFactory = scopeFactory;           
        }

        protected override async Task<bool> ProcessMessageAsync(string message)
        {
            try
            {
                using var scope = _scopeFactory.CreateScope();
                var dbContext = scope.ServiceProvider.GetRequiredService<IDbContext>();
                //прием от клиента
                try
                {
                    var input = JsonSerializer.Deserialize<DishRabbit>(message);
                    var existOrder = dbContext.Orders.FirstOrDefault(o => o.Id == input.OrderId);
                    if (existOrder != null)
                    {
                        existOrder.Status = "New";
                    }
                    else
                    {
                        dbContext.Orders.Add(new Order
                        {
                            Id = input.OrderId,
                            Status = "New",
                            User = JsonSerializer.Serialize(input.User),
                            ClientId = input.User.Id,
                            Dishes = JsonSerializer.Serialize(input.ListDish)
                        });
                    }
                    await dbContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    try
                    {
                        //прием от кухни
                        var inputOrder = JsonSerializer.Deserialize<Order>(message);
                        var client = JsonSerializer.Deserialize<UserDto>(inputOrder.User);
                        inputOrder.ClientId = client.Id;
                        var existOrder = dbContext.Orders.FirstOrDefault(o => o.Id == inputOrder.Id);
                        if (existOrder != null)
                        {
                            existOrder.Status = inputOrder.Status;
                        }
                        else
                        {
                            dbContext.Orders.Add(inputOrder);
                        }
                        await dbContext.SaveChangesAsync();
                    }
                    catch (Exception exIn)
                    {
                        throw exIn;
                    }

                }
                //var orderToSave = new Order()
                //{
                //    User = input.User,
                //    Status = input.Sta,
                //    Id = (int)input.OrderId,
                //    Dishes = JsonSerializer.Serialize(input.ListDish)
                //};
                //foreach (var item in input.ListDish)
                //{
                //orderToSave.Dishes.Add(new Dish
                //{
                //    DishId = item.DishId,
                //    DishDescpription = item.DishDescpription,
                //    DishQuantity = item.DishQuantity,
                //    DishImg = item.DishImg,
                //    DishName = item.DishName,
                //    DishPrice = item.DishPrice,
                //    Status = "New",
                //});
                //}
                Debug.WriteLine("success recive " + message);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("error recive " + ex.Message);
                return false;
            }
            //.Ack(ea.DeliveryTag);
            // using var scope = _serviceProvider.CreateScope();
            //var orderRepository = scope.ServiceProvider.GetService<Order>();
        }
    }
}
