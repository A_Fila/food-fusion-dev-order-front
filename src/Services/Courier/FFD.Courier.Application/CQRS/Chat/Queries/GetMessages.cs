﻿using AutoMapper;
using FDD.Shared.Interfases;
using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FFD.Courier.Core.Entities.Dto;
using FFD.Shared.Models;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace FFD.Courier.Application.CQRS.Chat.Command
{
    public class GetMessageQuery : IRequest<IEnumerable<MessageDto>>
    {
        public int? OrderId { get; set; }       
    }
    public class GetMessageQueryHandler : IRequestHandler<GetMessageQuery, IEnumerable<MessageDto>>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;
        public GetMessageQueryHandler(IDbContext dbContext, IMapper mapper, IUserAccessor userAccessor)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }
        public async Task<IEnumerable<MessageDto>> Handle(GetMessageQuery request, CancellationToken cancellationToken)
        {
            // Order? order = _dbContext.Orders.FirstOrDefault(x => x.Id == request.OrderId);
            var order = await _dbContext.Orders.FirstOrDefaultAsync(x => x.Id == request.OrderId && (x.ClientId == _userAccessor.Id || x.CourierId == _userAccessor.Id || _userAccessor.Role == "Admin"));
            if (order == null)
            {
                throw new ArgumentException();
            }
            var client = JsonSerializer.Deserialize<UserDto>(order.User);          

            var messages = _dbContext.Chats.Where(x => x.OrderId == request.OrderId).OrderBy(x => x.DataRecord).ToList();
            var result = _mapper.Map<IEnumerable<MessageDto>>(messages);
            foreach (var message in result)
            {
                message.Client = $"{client.FirstName} {client.LastName}";
            }
            return result;
        }
    }
    public sealed class GetMessageQueryValidator : AbstractValidator<GetMessageQuery>
    {
    }
}
