﻿using AutoMapper;
using FDD.Shared.Interfases;
using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Application.CQRS.Chat.Command
{
    public class SendMessageCommand : IRequest<bool>
    {
        public int? OrderId { get; set; }
        public string? Message { get; set; }
    }
    public class SendMessageCommandHandler : IRequestHandler<SendMessageCommand, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;
        public SendMessageCommandHandler(IDbContext dbContext, IMapper mapper, IUserAccessor userAccessor)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }

        public async Task<bool> Handle(SendMessageCommand request, CancellationToken cancellationToken)
        {
            Order? order = _dbContext.Orders.FirstOrDefault(x => x.Id == request.OrderId);
            if (order == null)
            {
                throw new ArgumentException();
            }
            _dbContext.Chats.Add(new Core.Entities.Db.Chat()
            {
                DataRecord = DateTime.Now,
                CourierId = order.CourierId,
                Message = request.Message,
                ClientId = Guid.NewGuid(),
                OrderId = order.Id
            });
            _dbContext.SaveChangesAsync();
            return true;
        }
    }
    public sealed class SendMessageCommandValidator : AbstractValidator<SendMessageCommand>
    {
    }
}
