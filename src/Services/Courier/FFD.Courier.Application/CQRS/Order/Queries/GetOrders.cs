﻿using AutoMapper;
using FDD.Shared.Interfases;
using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Application.CQRS.Chat.Command
{
    public class GetOrdersCommand : IRequest<List<Order>>
    {
      
    }
    public class GetOrdersCommandHandler : IRequestHandler<GetOrdersCommand, List<Order>>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;
        public GetOrdersCommandHandler(IDbContext dbContext, IMapper mapper, IUserAccessor userAccessor)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }
        public async Task<List<Order>> Handle(GetOrdersCommand request, CancellationToken cancellationToken)
        {
            List<Order> orders = _dbContext.Orders.Where(x => x.CourierId == null).ToList();            
            return orders;
        }
    }
    public sealed class GetOrdersCommandValidator : AbstractValidator<GetOrdersCommand>
    {
    }
}
