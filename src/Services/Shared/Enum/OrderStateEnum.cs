﻿using System.ComponentModel;

public enum OrderStateEnum {
    [Description("отменен")]
    Canceled = 1,

    [Description("открыт")]
    Open = 2,

    [Description("оплачен")]
    Paid = 3,

    [Description("приготовлен")]
    Ready = 4,

    [Description("доставлен")]
    Delivered = 5
}