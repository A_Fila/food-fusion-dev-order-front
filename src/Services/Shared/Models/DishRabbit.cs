﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace FFD.Shared.Models
{
    public class DishRabbit
    {
        public UserDto? User { get; set; }
        public List<ListDish>? ListDish { get; set; }
        public long OrderId { get; set; }

    }
    public class ListDish
    {
        [JsonPropertyName("dishId")]
        public int DishId { get; set; }
        [JsonPropertyName("dishImg")]
        public int DishImg { get; set; }
        [JsonPropertyName("dishName")]
        public string DishName { get; set; }
        [JsonPropertyName("dishPrice")]
        public int DishPrice { get; set; }
        [JsonPropertyName("dishDescpription")]
        public string DishDescpription { get; set; }
        [JsonPropertyName("dishQuantity")]
        public int DishQuantity { get; set; }
        [JsonPropertyName("status")]
        public string? Status { get; set; }
    }   
}
