﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using FFD.Shared.Models;

namespace FFD.Shared.Models
{
    public class OrderShared
    {
        public long Id { get; set; }
        public string? User { get; set; }        
        public string Status { get; set; }
        public string? ReadinessPercent { get; set; }
        public Guid? CookId { get; set; }
        public Guid? CourierId { get; set; }
        public string Dishes { get; set; }
        public Guid? ClientId { get; set; }
        //public ICollection<Dish> Dishes { get; set; }
        // public List<Dish>? ListDish { get; set; }
    }    
}
