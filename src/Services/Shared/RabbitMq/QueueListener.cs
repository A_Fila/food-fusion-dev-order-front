﻿using FFD.Shared.RabbitMq.Settings;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace FFD.Shared.RabbitMq
{
    public abstract class QueueListener : BackgroundService
    {
        private readonly TimeSpan _listenTimeout = TimeSpan.FromSeconds(10);
        private readonly BrokerSettings _brokerSettings;
        private readonly ReceiverSettings _queueSettings;

        public QueueListener(BrokerSettings brokerSettings, ReceiverSettings queueSettings)
        {
            _brokerSettings = brokerSettings;
            _queueSettings = queueSettings;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var factory = new ConnectionFactory()
                {
                    HostName = _brokerSettings.Host,
                    Port = _brokerSettings.Port,
                    UserName = _brokerSettings.User,
                    Password = _brokerSettings.Password
                };
                using var connection = factory.CreateConnection();
                using var channel = connection.CreateModel();

                channel.QueueDeclare(
                    queue: _queueSettings.Queue,
                    durable: _queueSettings.Durable,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                if (!string.IsNullOrEmpty(_queueSettings.Exchange))
                {
                    AddWithExchange(channel);
                }

                Consume(channel);

                await Task.Delay(_listenTimeout, stoppingToken);
            }
        }

        private void AddWithExchange(IModel channel)
        {
            _queueSettings.Keys = _queueSettings.RoutingKeys?.Split(';')?.ToList();
            channel.ExchangeDeclare(exchange: _queueSettings.Exchange,
                                  type: _queueSettings.ExchangeType);
            if (_queueSettings.Keys != null && _queueSettings.Keys.Any())
            {
                foreach (var key in _queueSettings.Keys)
                {
                    channel.QueueBind(
                        queue: _queueSettings.Queue,
                        exchange: _queueSettings.Exchange,
                        routingKey: key,
                        arguments: null);
                }
            }
            else
            {
                channel.QueueBind(
                    queue: _queueSettings.Queue,
                    exchange: _queueSettings.Exchange,
                    routingKey: "",
                    arguments: null);
            }
        }

        private void Consume(IModel channel)
        {
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += async (model, ea) =>
            {
                string error = "";
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                
               
                var success = await ProcessMessageAsync(message);
                //if (success)
                //    Ack(channel, ea.DeliveryTag);
            };
            channel.BasicConsume(queue: _queueSettings.Queue,
                                    autoAck: _queueSettings.AutoAck,
                                    consumer: consumer);
        }

        protected abstract Task<bool> ProcessMessageAsync(string message);

       //protected virtual void Ack(IModel channel, ulong deliveryTag)
       // {
       //     channel.BasicAck(deliveryTag: deliveryTag, multiple: false);
       // }
    }
}
