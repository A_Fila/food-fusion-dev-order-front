import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';
import svgr from 'vite-plugin-svgr';
import mkcert from 'vite-plugin-mkcert';
import basicSsl from '@vitejs/plugin-basic-ssl';
export default defineConfig({
    plugins: [svgr({ exportAsDefault: true }), react(), mkcert(), basicSsl()],
    resolve: {
        alias: [{ find: '@', replacement: '/src' }],
    },
    server: {
        https: false,
        proxy: {
            '/api': {
                target: 'http://localhost:8181',
                changeOrigin: true,
                secure: false,
            },
            '/tt': {
                //target: 'https://localhost:7257',
                target: 'http://localhost:8181',
                changeOrigin: true,
                secure: false,
                rewrite: path => path.replace(/^\/tt/, '')
            },
        },
    },
});


