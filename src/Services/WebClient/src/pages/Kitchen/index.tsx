import React, { useState, useEffect } from 'react';
import { Row, Col, Input, Select, Button, Modal, Radio, Avatar, Badge, message, Table } from 'antd';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { getCurrentUser } from '@/features/selectors/currentUser';
import { Header } from 'rsuite';
import { Search } from 'react-router-dom';
import Anonim from '../../images/Anonim.png';

function Page() {
    const [records, setRecords] = useState([]);
    const currentUser = useSelector(getCurrentUser);

    useEffect(() => {
        loadOrders();
    }, []);

    const loadOrders = () => {
        axios.get('/api/Kitchen/Kitchen/GetActualOrder')
            .then(response => {
                setRecords(response.data);
            });
    }

    const columns = [
        { title: 'ID', dataIndex: 'id', key: 'id' },
        {
            title: 'User', dataIndex: 'user', key: 'user', render: (text: any) => {
                const user = JSON.parse(text);
                return `${user.FirstName} ${user.LastName}`;
            }
        },
        { title: 'Status', dataIndex: 'status', key: 'status' },
        {
            title: 'Action',
            key: 'action',
            render: (text: any, record: any) => (
                <Button onClick={() => handleButtonClick(record.id)} type="primary" >
                    Выполнить
                </Button>
            ),
        },
    ];

    const handleButtonClick = (id: any) => {
        axios.post('api/kitchen/Kitchen/SetStatusCookedToOrder?id=' + id)
            .then(response => {
                console.log('Request sent successfully', response);
                loadOrders();
            })
            .catch(error => {
                console.error('Error sending request', error);
            });
    };

    const expandedRowRender = (record: any) => {
        const dishes = JSON.parse(record.dishes);
        const columns = [
            { title: 'Dish Name', dataIndex: 'dishName', key: 'dishName' },
            { title: 'Dish Quantity', dataIndex: 'dishQuantity', key: 'dishQuantity' },
        ];
        return <Table columns={columns} dataSource={dishes} pagination={false} rowKey="dishId" />;
    };

    return (
        <div>
            <Header style={{ display: 'flex', alignItems: 'center', backgroundColor: "#FF6F06", paddingLeft: 20, height: 60 }}>
                <h3 style={{ color: 'white', marginRight: 'auto' }}>Сервис кухни</h3>
                <Avatar className='ant-avatar' src={currentUser.avatar == 'undefined' || currentUser.avatar == null ? Anonim : `${currentUser.avatar}`} />
            </Header>
            <Table dataSource={records} columns={columns} expandable={{ expandedRowRender }} rowKey="id" />
        </div>
    );
}

export default Page;
