import React, { useCallback, useState } from 'react';
import { Panel, IconButton, Stack, Divider } from 'rsuite';
import { Link, useNavigate } from 'react-router-dom';
import GithubIcon from '@rsuite/icons/legacy/Github';
import FacebookIcon from '@rsuite/icons/legacy/Facebook';
import GoogleIcon from '@rsuite/icons/legacy/Google';
import WechatIcon from '@rsuite/icons/legacy/Wechat';
import { useAppDispatch } from '@/app/hooks';
import { loginByUsername } from '@/features/selectors/loginByUsername';
import Background from '../../images/background-verde.jpg';
import { Modal, Form, Input, Button, Select } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import axios from 'axios';

export interface FormValue {
  username: "",
  password: ""
}

const SignIn = () => {
  const navigate = useNavigate();
  const dispatch = useAppDispatch();
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false);
  const [errorText, setErrorText] = useState<string>("");
  const [form] = Form.useForm();
  const [formLogin] = Form.useForm();
  const [loginInfo, setloginInfo] = useState<any>({
    formValue: {
      username: "",
      password: "",
      isInternalUser: false
    }
  })

  const login = loginInfo.login;
  const password = loginInfo.password;
  const isInternalUser = false;
  const authLogin = useCallback(async () => {
    const { login, password } = formLogin.getFieldsValue(); // Получает данные из формы
    const result = await dispatch(loginByUsername({ login, password, isInternalUser }));
    if (result.meta.requestStatus === 'fulfilled') {
      // navigate(getRouteHome())
      navigate("/");;
    }
    else{
      setErrorText("Пользователь или пароль не найдены");
    }
  }, [dispatch, loginInfo.password, loginInfo.login, navigate, isInternalUser]);
  console.log('handleSave');
  // function onFinish(values: any): void {
  //   throw new Error('Function not implemented.');
  // }

  async function onClickSave(e: any): Promise<void> {
    try {
      setErrorText('');
      const formData = form.getFieldsValue(); // Получает данные из формы
      // Отправка данных на сервер
      const response = await axios.post('/api/Auth/register', formData);
      if (response.data.result == 200) {
        console.log('Registration successful:', response.data);
        setIsModalVisible(false);
      }
      else {
        setErrorText(response.data.message);
      }
    } catch (error) {
      console.error('Registration failed:', error);
    }
  }

  function onCloseModal(e: any): void {
    setIsModalVisible(false);
  }
  const handleSignUpClick = () => {
    setIsModalVisible(true);
  };
  // userService.getList();
  //--
  // authService.login(loginInfo.login, loginInfo.password, false).then(
  //   () => {
  //     // userService.getList();
  //     navigate("/profile");
  //     //window.location.reload();
  //   },
  //   (error) => {
  //     const resMessage =
  //       (error.response &&
  //         error.response.data &&
  //         error.response.data.message) ||
  //       error.message ||
  //       error.toString();
  //   }
  //   )
  //--
  //}
  //  setIsModalVisible(true);
  return (
    <div>
      <Stack
        justifyContent="center"
        alignItems="center"
        direction="column"
        style={{
          height: '100vh', backgroundImage: `url(${Background})`
        }}
      >
        {/* <Brand style={{ marginBottom: 10 }} /> */}

        <Panel bordered style={{ background: '#fff', width: 400 }} header={"Зайти на сайт"}>
          <p style={{ marginBottom: 10 }}>
            <span className="text-muted">Еще не были у нас? </span>{' '}
            <Link to="/sign-up" onClick={handleSignUpClick}>Зарегистрироваться</Link>
          </p>

          <Form
            name="login-form"
            form={formLogin}
            initialValues={{ remember: true }}
          // onFinish={onFinish}
          >
            <Form.Item
              name="login"
              rules={[{ required: true, message: 'Пожалуйста, введите логин или почту!' }]}
            >
              <Input prefix={<UserOutlined />} placeholder="Логин или почта" />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[{ required: true, message: 'Пожалуйста, введите пароль!' }]}
            >
              <Input.Password prefix={<LockOutlined />} placeholder="Пароль" />
            </Form.Item>
             {errorText && <Form.Item><div style={{ color: 'red' }}>{errorText}</div></Form.Item>}
           
            <Form.Item>
              <Button type="primary" htmlType="submit" onClick={authLogin}>
                Войти
              </Button>
              <a href="#" style={{ float: 'right' }}>Забыл пароль?</a>
            </Form.Item>
          </Form>
        </Panel>
      </Stack>
      <Modal
        title={'Регистрация пользователя'}
        open={!!isModalVisible}
        onOk={onClickSave}
        onCancel={onCloseModal}
        okText='Зарегистрироваться'
        cancelText='Отмена'
      >
        <Form
          form={form}
          name="basic"
          initialValues={{ remember: true }}
          labelCol={{ span: 6 }}
          wrapperCol={{ span: 14 }}
        // layout="horizontal"        
        // style={{ maxWidth: 600 }}
        >
          <Form.Item
            label="Фамилия"
            name="firstName"
            rules={[{ required: true, message: 'Please input your first name!' }]}              >
            <Input />
          </Form.Item>
          <Form.Item
            label="Имя"
            name="lastName"
            rules={[{ required: true, message: 'Please input your last name!' }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label="Отчество"
            name="middleName"
            rules={[{ required: false, message: 'Please input your middle name!' }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label="Логин"
            name="login"
            rules={[{ required: true, message: 'Please input your login!' }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label="Email"
            name="email"
            rules={[{ required: true, message: 'Please input your email!', type: 'email' }]}>
            <Input />
          </Form.Item>
          <Form.Item
            label="Пароль"
            name="password"
            rules={[{ required: false, message: 'Please input your password!' }]}>
            <Input type='password' />
          </Form.Item>
          <Form.Item
            label="Адрес"
            name="address"
            rules={[{ required: false, message: 'Please input your address!' }]}>
            <Input />
          </Form.Item>
          {errorText && <div style={{ color: 'red' }}>{errorText}</div>}
        </Form>
      </Modal>
    </div>
  );
};

export default SignIn;
