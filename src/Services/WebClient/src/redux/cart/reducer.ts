import { PayloadAction, createSlice } from "@reduxjs/toolkit";

interface CartState {
    itemsInCart: DishDataType[];
}

const initialState: CartState = {
    itemsInCart: []
};

const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addItemInCart: (state, action: PayloadAction<DishDataType>) => {
            const itemIndex = state.itemsInCart.findIndex(item => item.id === action.payload.id);
            if (itemIndex !== -1) {
                state.itemsInCart[itemIndex].quantity++;
                state.itemsInCart[itemIndex].summ = state.itemsInCart[itemIndex].price * state.itemsInCart[itemIndex].quantity;
            } else {
                //action.payload.quantity = action.payload.quantity !== undefined ? action.payload.quantity : 1;
                //state.itemsInCart.push(action.payload);
                // state.itemsInCart[itemIndex].summ = action.payload.price * action.payload.quantity;
                state.itemsInCart.push({ 
                    ...action.payload, 
                    quantity: action.payload.quantity !== undefined ? action.payload.quantity : 1, 
                    summ:action.payload.price * (action.payload.quantity !== undefined ? action.payload.quantity : 1) });
            }
        },
        deleteItemFromCart: (state, action: PayloadAction<number>) => {
            const index = state.itemsInCart.findIndex(item => item.id === action.payload);
            if (index !== -1) {
                if (state.itemsInCart[index].quantity > 1) {
                    state.itemsInCart = state.itemsInCart.map((item, idx) => {
                        if (idx === index) {
                            return { ...item, quantity: item.quantity - 1, summ: state.itemsInCart[index].price * (item.quantity - 1) };
                        }
                        return item;
                    });
                } else {
                    state.itemsInCart = state.itemsInCart.filter((_, idx) => idx !== index);
                }
            }
        },
        deleteItemsFromCart: (state, action: PayloadAction<number[]>) => {
            state.itemsInCart = state.itemsInCart.filter(dish => !action.payload.includes(dish.id));
        },
        clearItemsFromCart: (state, action: PayloadAction) => {
            state.itemsInCart = [];
        }
    }
});

export const { addItemInCart, deleteItemFromCart, deleteItemsFromCart, clearItemsFromCart} = cartSlice.actions;
export default cartSlice.reducer;