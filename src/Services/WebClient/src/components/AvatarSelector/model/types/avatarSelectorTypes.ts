export interface AvatarSelectorProps {
    onChange?: (s: string | undefined) => void;
    userAvatar?: string;
}