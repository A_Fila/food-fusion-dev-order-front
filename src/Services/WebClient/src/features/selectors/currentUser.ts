import { RootState } from "../../app/store";

export const getUserInited = (state: RootState) => state.user.inited;
export const getUserAuth = (state: RootState) => state.user.signed;
export const getUserIsLoading = (state: RootState) => state.user.isLoading;
export const getCurrentUser = (state: RootState) => state.user.currentUser;
