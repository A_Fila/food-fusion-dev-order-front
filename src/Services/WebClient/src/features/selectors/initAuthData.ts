import { USER_LOCALSTORAGE_TOKEN } from '@/constants';
import { IUserResponse, ThunkConfig } from '@/interfaces/common';
import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosError } from 'axios';
// import { IUserResponse, ThunkConfig, User } from '../../../../interfaces/common';
// import { USER_LOCALSTORAGE_TOKEN } from '../../../../constants/constants';

export const initAuthData = createAsyncThunk<
IUserResponse,
    void,
    ThunkConfig<AxiosError>
>('user/initAuthData', async (articleId, thunkApi) => {
    const { extra, rejectWithValue } = thunkApi;

    try {
        const response = await extra.api.get<IUserResponse>('/Auth/user', {
            withCredentials: true,
        });
        axios.interceptors.request.use((config) => {
            config.headers!.authorization = `Bearer ${localStorage.getItem(
                USER_LOCALSTORAGE_TOKEN,
            )}`;
            return config;
        });

        if (!response.data) {
            throw new Error();
        }

        return response.data;
    } catch (e) {
        const typedError = e as AxiosError;
        return rejectWithValue(typedError);
    }
});
