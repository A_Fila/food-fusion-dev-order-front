// import { HttpRequestError } from "@pnp/odata";
import { createSlice, PayloadAction } from "@reduxjs/toolkit";
// import { ICurrentUser, IUserResponse, User } from "../interfaces/common";
// import { USER_LOCALSTORAGE_EMAIL, USER_LOCALSTORAGE_SIGNED, USER_LOCALSTORAGE_TOKEN } from "../constants/constants";
import { initAuthData } from "./initAuthData";
import { ICurrentUser, IUserResponse, User } from "@/interfaces/common";
import { USER_LOCALSTORAGE_SIGNED, USER_LOCALSTORAGE_TOKEN, USER_LOCALSTORAGE_EMAIL } from "@/constants";
// import { initAuthData } from "../services/initAuthData";
// import {ICurrentUser} from "../interfaces/common";

export interface UserSchema {
  currentUser: User;
  requiredChangePassword: boolean;

  isLoading: boolean;
  inited: boolean;
  signed: boolean;
  error?: string;
}

const initialState: UserSchema = {
  currentUser: null,
  // isCurrentUserGroupsLoading: false,
  // isCurrentUserGroupsError: "",
  // currentUserGroups: [],
  signed:!!localStorage.getItem(USER_LOCALSTORAGE_SIGNED),
  inited:false,
  isLoading: false,
  requiredChangePassword: false,
};

export const userSlice = createSlice({
  name: "currentUser",
  initialState,
  reducers: {
    setAuthData: (state, { payload }: PayloadAction<User>) => {
      state.currentUser = payload;
  },
    login: (state) => {
      state.signed = true;
      localStorage.setItem(USER_LOCALSTORAGE_SIGNED, 'true');
  },
  logout: (state) => {
    state.currentUser = undefined;
    localStorage.removeItem(USER_LOCALSTORAGE_TOKEN);
    localStorage.removeItem(USER_LOCALSTORAGE_SIGNED);
    window.location.href = '/';
},
    setCurrentUser: (state, action: PayloadAction<ICurrentUser>) => {
      // state.currentUser = action.payload;
    },
    setUserInfo: (
      state,
      {
          payload,
      }: PayloadAction<{
          requiredChangePassword: boolean;
          userId: string;
      }>,
  ) => {
      state.requiredChangePassword = payload.requiredChangePassword;
      state.currentUser = {
          id: payload.userId,
          email: '',
          // roles: [],         
          displayNameRu: '',
          login: '',
          avatar: '',          
      };
  },
    // setIsCurrentUserGroupsLoadingLoading(state) {
    //   state.isCurrentUserGroupsLoading = true;
    // },
    // setCurrentUserGroups: (state, action: PayloadAction<string[]>) => {
    //   state.isCurrentUserGroupsLoading = false;
    //   state.isCurrentUserGroupsError = "";
    //   state.currentUserGroups = action.payload;
    // },
    // setIsCurrentUserGroupsError(state, action: PayloadAction<string>) {
    //   state.isCurrentUserGroupsLoading = false;
    //   state.isCurrentUserGroupsError = action.payload;
    // },
  },
  extraReducers: (builder) => {
      builder.addCase(initAuthData.pending, (state) => {
          state.isLoading = true;
      });
      builder.addCase(
          initAuthData.fulfilled,
          (state, { payload }: PayloadAction<IUserResponse>) => {
              state.currentUser = payload.body;
              state.inited = true;
              state.isLoading = false;                  
              localStorage.setItem(USER_LOCALSTORAGE_EMAIL, payload.body?.email);
          },
      );
      builder.addCase(initAuthData.rejected, (state, action) => {
          state.inited = true;
          state.isLoading = false;
          state.error = action.payload?.message;
      });
  },
  
});

// export const { setCurrentUser } = userSlice.actions;

// export default userSlice.reducer;
export const { actions: userActions } = userSlice;
export const { reducer: userReducer } = userSlice;
