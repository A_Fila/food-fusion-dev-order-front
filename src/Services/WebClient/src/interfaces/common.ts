import { MenuDividerType, MenuItemGroupType, MenuItemType, SubMenuType } from "antd/es/menu/hooks/useItems";
import { AxiosInstance } from "axios";
export type IResponseModel<T> = {
    result: number,
    message: string,
    body: T,
  }

export type ILoginResponse = IResponseModel<IResponse>

export interface IResponse {
    accessToken: string;
    refreshToken: string;
    login: string;
    avatar: string;
    requiredChangePassword: boolean;
    userId: string;
  }
  export interface ThunkExtraArg {
    api: AxiosInstance;
  }

  export interface ICurrentUser {
    EUFormat: string;
    Title: string;
    Email: string;
    Avatar: string;
  }

  export interface User {
    id: string;
    email: string;
    // roles: UserRole[];
    displayNameRu: string;    
    login: string;
    avatar: string;
    role: string;
  }

  export type IUserResponse = IResponseModel<User>

  export interface ThunkConfig<T> {
    rejectValue: T;
    extra: ThunkExtraArg;
    state: any;
  }

  export type ExtendedItemType<T extends ExtendedMenuItemType = ExtendedMenuItemType> = T | SubMenuType<T> | MenuItemGroupType<T> | MenuDividerType | null;

  export interface LayoutsProps {
    menuItems: ExtendedItemType<ExtendedMenuItemType>[];
  }
  export interface ExtendedMenuItemType extends MenuItemType {
    extraField1: string;
    extraField2: number;
  }