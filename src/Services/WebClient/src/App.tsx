import React, { Suspense, useEffect } from 'react';
import logo from './logo.svg';
import './App.css';
import './styles/Layouts.css';
import { getUserAuth, getUserInited, getUserIsLoading } from './features/selectors/currentUser';
import { Routes, Route, useSearchParams, NavLink } from 'react-router-dom';
import Frame from './components/Frame/Frame';
import { MenuProps, Spin } from 'antd';
import SignInPage from './pages/signIn';
import { useAppDispatch } from './app/hooks';
import { Provider, useSelector } from 'react-redux';
import { initAuthData } from './features/selectors/initAuthData';
import '../node_modules/rsuite/dist/rsuite.min.css';
import UsersPage from './pages/Users';
import MenuGridPage from './pages/MenuGrid';
import MenuListPage from './pages/MenuList';
import ChatPage from './pages/Chat';
import MenuTable from './components/Menu/menu-table';
import { store } from "./app/store";

const App = () => {
  const [serchParams] = useSearchParams();
  const dispatch = useAppDispatch();
  const inited = useSelector(getUserInited);
  const signed = useSelector(getUserAuth);
  const isLoading = useSelector(getUserIsLoading);
  const passwordResetToken = serchParams.get('resettoken');
  const personId = serchParams.get('id');

  useEffect(() => {
    if (!inited && signed) {
      dispatch(initAuthData());
    }
  }, [dispatch, inited, signed]);

  useEffect(() => {
  }, []);

  if (isLoading) {
    return   <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', height: '100vh' }}>
    <Spin spinning={isLoading} size="large" />
  </div>; //todo loader
  }

  const items: MenuProps['items'] = [
    {
      key: 'nav1',
      label: <NavLink to='Users' >Users</NavLink>,//'nav 1',
    },
    {
      key: 'nav2',
      label: <NavLink to='MenuList' >MenuList</NavLink>,//'nav 2',
    },
    {
      key: 'nav3',
      label: <NavLink to='MenuGrid' >MenuGrid</NavLink>,//'nav 3',
    },
  ];

  return (
    //  <CustomProvider locale={enGB}>
    <Provider store ={store}>
      <Suspense fallback="">
        <div>
          {
            signed && inited ? (//! && passwordResetToken && !personId ? (              
              <Routes>                
                <Route path="/" element={<Frame menuItems={items} />}/>
                  <Route path="/Users" element={<div><UsersPage /></div>} />
                  <Route path="/MenuList" element={<div><MenuListPage/></div>} />
                  <Route path="/MenuGrid" element={<div><MenuGridPage /></div>} />
                  <Route path="/Chat/:chatId" element={<ChatPage />} />
                  {/* <Route path="/*" element={<ShareApp />} /> */}
                  {/* <Route path="*" element={<Error404Page />} /> */}
                {/* </Route> */}
              </Routes>
            ) : (
              <SignInPage />
            )
          }
        </div>
      </Suspense>
    </Provider>

    // </CustomProvider>
  );
}
export default App;