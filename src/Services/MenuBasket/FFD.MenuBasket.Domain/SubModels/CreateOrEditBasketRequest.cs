﻿using System.Collections.Generic;

namespace FFD.MenuBasket.Domain.SubModels
{
    public class CreateOrEditBasketRequest
    {
        public bool IsAdd { get; set; }
        public int DishId { get; set; }
        //public List<int> DishIds { get; set; } = new();
    }
}
