﻿using FFD.MenuBasket.DataAccess.EntityFramework;
using FFD.MenuBasket.DataAccess.Repositories;
using FFD.MenuBasket.Domain.Entities;
using FFD.MenuBasket.Domain.SubModels;
using FFD.Shared.Models;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;

namespace FFD.MenuBasketDataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly MyDatabaseContext _dbContext;
        public EfRepository(DbContext dbContext)
        {
            _dbContext = dbContext as MyDatabaseContext;
        }
        //Common
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbContext.Set<T>().OrderBy(x=>x.Id).ToListAsync();
        }
        public async Task<T> GetByIdAsync(int id)
        {
            //TODO Logging this
            return await _dbContext.Set<T>().FindAsync(id) ?? throw new Exception($"Dish с {id} не найден");
        }
        //Dish
        public Task<int> AddNewDishAsync(Dish dish)
        {
            Dish addedDish = _dbContext.Dishes.Add(dish).Entity;
            _dbContext.SaveChanges();
            return Task.FromResult(addedDish.Id);
        }
        public Task<bool> DeleteDishByIdAsync(int id)
        {
            Dish? editedDish = _dbContext.Dishes.FirstOrDefault(item => item.Id.Equals(id));

            if (editedDish == null)
                return Task.FromResult(false);

            _dbContext.Remove(editedDish);
            _dbContext.SaveChanges();

            return Task.FromResult(true);
        }
        //Basket
        public Task<int> PostNewBasketWithDishesAsync(Basket basket, params CreateBasketWithDishAndQuantities[]? req)
        {
            if (_dbContext.Baskets.Any(b => b.CustomerId == basket.CustomerId)) return Task.FromResult(basket.Id);

            Basket addedBasket = _dbContext.Baskets.Add(basket).Entity;
            _dbContext.SaveChanges();

            if (req.FirstOrDefault() != default)
            {
                foreach (var v in req)
                {
                    Dish? dish = _dbContext.Dishes.FirstOrDefault(item => item.Id.Equals(v.DishId));

                    if (dish == null)
                        continue;

                    DishBasket diba = new()
                    {
                        BasketId = basket.Id,
                        DishId = dish.Id,
                        DishQuantity = v.DishQuantity
                    };
                    _dbContext.DishBaskets.Add(diba);
                }
            }

            _dbContext.SaveChanges();
            return Task.FromResult(addedBasket.Id);
        }
        public Task<bool> DeleteBasketbyIdAsync(int id)
        {
            Basket? editedBasket = _dbContext.Baskets.FirstOrDefault(item => item.Id.Equals(id));

            if (editedBasket == null)
                return Task.FromResult(false);

            _dbContext.Remove(editedBasket);
            _dbContext.SaveChanges();

            return Task.FromResult(true);
        }
        public Task<bool> AddDishes2BasketAsync(int id, int dishId)
        {
            Basket? basket = _dbContext.Baskets.FirstOrDefault(item => item.Id.Equals(id));
            Dish? dish = _dbContext.Dishes.FirstOrDefault(item => item.Id.Equals(dishId));

            if (basket == null || dish == null)
                return Task.FromResult(false);

            //если добавляемое блюдо уже существует в текущей корзине, то просто увеличить счетчик
            DishBasket? focusDishBasket =
                _dbContext.DishBaskets.FirstOrDefault(item => item.BasketId.Equals(id) && item.DishId.Equals(dishId));

            if (focusDishBasket != default)
            {
                focusDishBasket.DishQuantity++;
            }
            else
            {
                DishBasket diba = new()
                {
                    DishQuantity = 1,
                    BasketId = basket.Id,
                    DishId = dish.Id
                };
                _dbContext.DishBaskets.Add(diba);
            }

            _dbContext.SaveChanges();
            return Task.FromResult(true);
        }
        public Task<bool> DeleteDishFromBasketAsync(int id, int dishId)
        {
            Basket? basket = _dbContext.Baskets.FirstOrDefault(item => item.Id.Equals(id));
            Dish? dish = _dbContext.Dishes.FirstOrDefault(item => item.Id.Equals(dishId));

            if (basket == null || dish == null)
                return Task.FromResult(false);


            DishBasket? focusDishBasket =
                _dbContext.DishBaskets.FirstOrDefault(item => item.BasketId.Equals(id) && item.DishId.Equals(dishId));

            if (focusDishBasket is { DishQuantity: > 1 }) //было focusDishBasket != default && focusDishBasket.DishQuantity >= 1
            {
                focusDishBasket.DishQuantity--;
            }
            else
            {
                _dbContext.Remove(focusDishBasket);
            }

            _dbContext.SaveChanges();
            return Task.FromResult(true);
        }
        public async Task<IEnumerable<Dish>> GetDishesByUserBasket(Guid userId)
        {
            var resultDishesList = new List<Dish>();

            var basket = _dbContext.Baskets.FirstOrDefault(x => x.CustomerId.Equals(userId));

            var dishBaskets = await _dbContext.DishBaskets.Where(x => basket != null && x.BasketId.Equals(basket.Id)).ToListAsync();

            foreach (var dishBasket in dishBaskets)
            {
                var dish = _dbContext.Dishes.FirstOrDefault(x => x.Id.Equals(dishBasket.DishId));
                if (dish != null)
                {
                    for (int i = 0; i < dishBasket.DishQuantity; i++)
                    {
                        resultDishesList.Add(dish);
                    }
                }
            }

            return resultDishesList;
        }
        public async Task<IEnumerable<Basket>> GetBasketsByUser(Guid userId)
        {
            var baskets = await _dbContext.Baskets.Where(x => x.CustomerId.Equals(userId)).Select(x=>x).ToListAsync();

            return baskets;
        }

        public async Task<long> SaveOrder(List<ListDish> list)
        {            
            Order order = new Order() {DishList= JsonSerializer.Serialize(list) };
            _dbContext.Orders.Add(order);
            await _dbContext.SaveChangesAsync(); 
            return order.Id;
        }
    }

}
