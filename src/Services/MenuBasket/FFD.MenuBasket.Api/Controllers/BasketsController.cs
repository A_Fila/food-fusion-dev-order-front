﻿using FDD.Shared.Interfases;
using FFD.MenuBasket.DataAccess.Repositories;
using FFD.MenuBasket.Domain.Entities;
using FFD.MenuBasket.Domain.SubModels;
using FFD.OrderService.WebHost.Notifier;
using FFD.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using FFD.MenuBasket.Api.Logging;

namespace FFD.MenuBasket.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BasketsController : ControllerBase
    {
        private readonly Notifier _notifier;
        private readonly IRepository<Basket> _basketRepo;
        private readonly IUserAccessor _userAccessor;
        private readonly ILogger<BasketsController> _logger;

        public BasketsController(IRepository<Basket> basketRepo, Notifier notifier, IUserAccessor userAccessor, ILogger<BasketsController> logger)
        {
            _basketRepo = basketRepo;
            _notifier = notifier;
            _userAccessor = userAccessor;
            _logger = logger;
        }

        /// <summary>
        /// Добавление новой КОРЗИНЫ С БЛЮДАМИ В БД
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> PostNewBasketAsync(params CreateBasketWithDishAndQuantities[]? req)
        {
            if (_userAccessor.Id != null)
            {
                Basket basket = new()
                {
                    CustomerId = _userAccessor.Id.Value,
                    CustomName = $"B{new Random().Next(300, 3000)}",
                    BasketCreatedDate = DateTime.Now.ToString("dd.MM.yyyy HH-mm-ss")
                };

                try
                {
                    var id = await _basketRepo.PostNewBasketWithDishesAsync(basket, req);
                    LoggingHelper.LogPostMethod<BasketsController, Basket>(_logger, basket, id);
                    return Ok(id);
                }
                catch (Exception e)
                {
                    LoggingHelper.LogControllerPostException(_logger, e);
                    throw;
                }
            }
            return Unauthorized();
        }
        /// <summary>
        /// Получить все блюда в Basket
        /// </summary>
        /// <returns></returns>
        [HttpGet("Dishes")]
        public async Task<ActionResult<List<Dish>>> GetByUserIdAsync()
        {
            if (_userAccessor.Id != null)
            {
                var userId = _userAccessor.Id.Value;

                var dishList = await _basketRepo.GetDishesByUserBasket(userId);

                return Ok(dishList);
            }
            return Unauthorized();
        }

        /// <summary>
        /// Получить список всех Baskets для юзера
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Basket>>> GetAllBaskets4UserAsync()
        {
            //var currentUser = HttpContext.User;
            //var userId = currentUser.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            //var dishes = await _basketRepo.GetAllAsync();
            //return Ok(dishes);

            if (_userAccessor.Id != null)
            {
                var userId = _userAccessor.Id.Value;
                var baskets = await _basketRepo.GetBasketsByUser(userId);
                LoggingHelper.LogGetMethod(_logger, baskets);
                return Ok(baskets);
            }
            return Unauthorized();
        }

        /// <summary>
        /// Удаление Basket по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteBasket(int id)
        {
            bool isSuccess = await _basketRepo.DeleteBasketbyIdAsync(id);

            if (!isSuccess)
                return NotFound(id);
            return Ok($"Basket с id = {id} удален.");
        }

        /// <summary>
        /// Добавить\удалить блюдо в корзине
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateDishOnBasketAsync(int id, CreateOrEditBasketRequest request)
        {
            if (request.IsAdd)
            {
                bool isSuccess = await _basketRepo.AddDishes2BasketAsync(id, request.DishId);

                if (!isSuccess)
                    return BadRequest("Несуществующая корзина или блюдо");

                return Ok($"В Basket {id} добавлено Dish {request.DishId}.");
            }
            else
            {
                bool isSuccess = await _basketRepo.DeleteDishFromBasketAsync(id, request.DishId);

                if (!isSuccess)
                    return BadRequest("Несуществующая корзина или блюдо");

                return Ok($"Удален Basket {id} с Dish {request.DishId}.");
            }
        }

        //send to order
        [HttpPost("SendToOrder"), Authorize]
        public async Task<IActionResult> SendToOrder(List<ListDish> listDish)
        {
            var orderId = await _basketRepo.SaveOrder(listDish);
            _notifier.NotifyMessage(listDish, orderId);
            return Ok(orderId);
        }
    }
}
