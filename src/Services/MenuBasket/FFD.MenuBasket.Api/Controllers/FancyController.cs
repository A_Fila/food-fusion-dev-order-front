﻿using Microsoft.AspNetCore.Mvc;
using System.IO;
namespace Otus.Backend.Controllers
{
    [Route("/api/[controller]")]
    public class FancyController : ControllerBase
    {
        [HttpGet("guid")]
        public string GetGuid()
        {
            return "Get " + Guid.NewGuid();
        }

        [HttpPut("guid")]
        public string PostGuid()
        {
            return "Post " + Guid.NewGuid();
        }
    }
}
